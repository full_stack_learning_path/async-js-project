const runProblem1 = require('./server/problem1/main-problem1');
const runProblem2 = require('./server/problem2/main-problem2');

// Execute Problem 1
runProblem1();

// Execution Problem 2 after delay of 24 seconds
setTimeout(() => {
  console.log();

  console.log('Executing problem2');

  console.log();

  runProblem2();
}, 24000);
