const path = require('path');
const { mkdirp } = require('mkdirp');

// user defined
const readFileByFileName = require('./tasks/1- readFile');
const writeFileAsync = require('./tasks/2- writeFile');
const toUppercaseAsync = require('./tasks/3- toUppercase');
const toLowercaseAndSplitAsync = require('./tasks/4- toLowercaseAndSplit');
const sortFileAsync = require('./tasks/5- sortFile');
const deleteFilesWithDelayAsync = require('./tasks/6- deleteFile');

// output directory - absolute path
const outputPath = path.join(__dirname, '../../public/output');

// Ensure that the output directory exists, including any intermediate directories
async function ensureOutputDirectoryExists() {
  try {
    await mkdirp(outputPath);
    console.log('Output directory created:', outputPath);
  } catch (error) {
    console.error('Error creating output directory:', error);
    throw error;
  }
}

module.exports = async function main() {
  // Ensures always directory exists
  ensureOutputDirectoryExists();

  try {
    // Step1 : Read lipsum.txt from data
    const lipsumText = await readFileByFileName('lipsum.txt');

    // Step2 : Convert to uppercase and write name uppercase
    const upperText = await toUppercaseAsync(lipsumText);
    const uppercaseFilePath = path.join(outputPath, 'uppercase.txt');
    const uppercaseFileName = await writeFileAsync(
      uppercaseFilePath,
      upperText
    );
    console.log(`Uppercase content written to ${uppercaseFilePath}`);

    // Step3 : Convert to lowercase, split into sentences, and write each sentence to separate files
    const lowercaseAndSplitText = await toLowercaseAndSplitAsync(lipsumText);
    const sentenceFileNames = [];

    for (let index = 0; index < lowercaseAndSplitText.length; index++) {
      const sentence = lowercaseAndSplitText[index];
      const fileName = `${outputPath}/sentence_${index}.txt`;

      await writeFileAsync(fileName, sentence);

      // Printing file name after writing successfully
      console.log(`File written to output folder: ${fileName}`);

      sentenceFileNames.push(fileName);
    }

    // Step4 : Read the new files, sort them, and write to sorted.txt
    const sortedText = await sortFileAsync([
      uppercaseFileName,
      ...sentenceFileNames,
    ]);
    await writeFileAsync(`${outputPath}/sorted.txt`, sortedText);

    // Step5 : Delete all the new files mentioned in filename.txt
    const filesToDelete = [
      uppercaseFileName,
      ...sentenceFileNames,
      `${outputPath}/sorted.txt`,
    ];
    await writeFileAsync(
      `${outputPath}/filenames.txt`,
      filesToDelete.join('\n')
    );
    await deleteFilesWithDelayAsync(filesToDelete);

    console.log('All tasks completed successfully.');
  } catch (error) {
    console.error('Error: ', error);
  }
};
