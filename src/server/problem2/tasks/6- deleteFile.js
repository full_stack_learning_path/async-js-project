const fs = require('fs').promises;

module.exports = async function deleteFilesWithDelayAsync(filePaths) {
  await new Promise((resolve) => {
    setTimeout(resolve, 8000);
  });
  const deletePromises = filePaths.map(async (file) => {
    try {
      await fs.unlink(file);
      console.log(`File deleted: ${file}`);
    } catch (error) {
      console.log(`Error deleting file: ${file}`, error);
    }
  });

  // Using Promise.allSettled too ensure that all files are deleted
  // even if some deletion operations fail.
  await Promise.allSettled(deletePromises);
};
