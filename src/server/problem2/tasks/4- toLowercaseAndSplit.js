// This function will split string into array of strings if it encounters
// any of dot(.), exclamation (!) or question marks(?).
// Filter will remove all empty strings, whitespaces as strings.

function splitSentences(text) {
  return text.split(/[.!?]/).filter(Boolean);
}

// Function to convert text to lowercase and split it into sentences using async/await
module.exports = async function toLowerAndSplitAsync(text) {
  const lowerText = text.toLowerCase();
  const sentences = splitSentences(lowerText);
  return Promise.resolve(sentences);
};
