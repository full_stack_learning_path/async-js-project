const fs = require('fs').promises;

// Function to write file asynchronously
module.exports = async function wrriteFileAsync(fileName, content) {
  // eslint-disable-next-line no-useless-catch
  try {
    await fs.writeFile(fileName, content, 'utf8');
    return fileName;
  } catch (err) {
    throw err;
  }
};
