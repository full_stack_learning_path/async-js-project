const fs = require('fs').promises;
const path = require('path');

// Function to sort files by file path
module.exports = async function sortFilesAsync(filePaths) {
  try {
    const fileContents = await Promise.all(
      filePaths.map(async (filePath) => {
        const content = await fs.readFile(filePath, 'utf8');
        return content.trim();
      })
    );

    // Sort the content
    fileContents.sort();

    return fileContents.join('\n');
  } catch (error) {
    console.error(error);
  }
};
