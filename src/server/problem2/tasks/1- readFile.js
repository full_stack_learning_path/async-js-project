const fs = require('fs').promises;
const path = require('path');

// Functions to read a file by filename
module.exports = async function readFileByFileName(fileName) {
  const filePath = path.join(__dirname, '../data/', fileName);
  return fs.readFile(filePath, 'utf8');
};
