const callback = require('./part1_Callback/app-callback');
const promise = require('./part2_Promise/app-promise');
const asyncAwait = require('./part3_Async_await/app-async-await');

// Execute problem1
module.exports = function runProblem1() {
  callback();

  // Delay for 8 seconds before calling promise()
  setTimeout(() => {
    promise();
  }, 8000);

  // Delay for 16 seconds before calling asyncAwait()
  setTimeout(() => {
    asyncAwait();
  }, 16000);
};
