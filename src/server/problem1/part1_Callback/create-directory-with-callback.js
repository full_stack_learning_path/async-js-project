const fs = require('fs');
const path = require('path');

module.exports = function createDirectoryWithCallback(callback) {
  const directoryPath = path.join(
    __dirname,
    '../../../public/output/async_js_exercises'
  );

  fs.mkdir(directoryPath, { recursive: true }, (err) => {
    if (err) {
      return callback(err);
    } else {
      console.log('Directory created using callback:', directoryPath);
      callback(null);
    }
  });
};
