const fs = require('fs');

// Delete a file with delay to see the working4
module.exports = function deleteFilesWithDelayCallback(file, callback) {
  setTimeout(() => {
    fs.unlink(file, (err) => {
      if (err) {
        return callback(err);
      } else {
        callback(null);
      }
    });
  }, 4000);
};
