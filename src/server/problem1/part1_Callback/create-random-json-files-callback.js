const fs = require('fs');
const path = require('path');

const directoryPath = path.join(
  __dirname,
  '../../../public/output/async_js_exercises'
);

// Create a random JSON file
module.exports = function createRandomJSONFile(index, callback) {
  const fileName = `file${index}.json`;
  const filePath = path.join(directoryPath, fileName);
  const jsonData = { data: Math.random() };

  fs.writeFile(filePath, JSON.stringify(jsonData), (err) => {
    if (err) {
      return callback(err);
    }
    callback(null, filePath);
  });
};
