const createDirectoryWithCallback = require('./create-directory-with-callback');
const createRandomJSONFilesCallback = require('./create-random-json-files-callback');
const deleteJSONFilesCallback = require('./delete-files-callback');

// Create the directory
module.exports = function runTask() {
  return createDirectoryWithCallback((err) => {
    if (err) {
      console.error('Error creating directory:', err);
      return;
    }

    const files = [];

    // Create 3 random JSON files programatically
    function createRandomFilesCallback(index) {
      createRandomJSONFilesCallback(index, (err, file) => {
        if (err) {
          console.error('Error creating file:', err);
          return;
        }
        files.push(file);

        if (files.length === 3) {
          console.log('Files created using callback:', files);

          // Step 3: Delete the files concurrently
          files.forEach((fileToDelete) => {
            deleteJSONFilesCallback(fileToDelete, (err) => {
              if (err) {
                console.error('Error deleting file:', err);
                return;
              }
              console.log('File deleted using callback:', fileToDelete);
            });
          });
        }
      });
    }

    for (let index = 1; index <= 3; index++) {
      createRandomFilesCallback(index);
    }
  });
};
