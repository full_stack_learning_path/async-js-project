const fs = require('fs').promises;

// Delete the files concurrently using async/await after a delay
module.exports = async function deleteFilesConcurrentlyAsync(files) {
  setTimeout(async () => {
    const deletePromises = files.map(async (file) => {
      await fs.unlink(file);
      console.log('File deleted using async:', file);
    });
    await Promise.all(deletePromises);
  }, 6000);
};
