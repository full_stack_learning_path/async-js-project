const fs = require('fs').promises;
const path = require('path');

const directoryPath = path.join(
  __dirname,
  '../../../public/output/async_js_exercises'
);

// Create the directory if it doesn't exist
module.exports = async function createDirectory() {
  await fs.mkdir(directoryPath, { recursive: true });

  console.log('Directory created using async-await:', directoryPath);
};
