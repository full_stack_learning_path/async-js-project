const createDirectoryWithAsync = require('./create-directory-with-async-await');
const createRandomJSONFilesAsync = require('./create-random-json-files-async-await');
const deleteJSONFilesWithAsync = require('./delete-files-async-await');

module.exports = async function main() {
  try {
    const directory = await createDirectoryWithAsync();

    const files = await createRandomJSONFilesAsync();

    console.log('Files created using async:', files);

    await deleteJSONFilesWithAsync(files);
  } catch (err) {
    console.error('Error:', err);
  }
};
