const fs = require('fs').promises;
const path = require('path');

const directoryPath = path.join(
  __dirname,
  '../../../public/output/async_js_exercises'
);

// Create 3 random JSON files programatically using async/await
module.exports = async function createRandomJSONFilesAsync() {
  const files = [];
  for (let i = 1; i <= 3; i++) {
    const fileName = `file${i}.json`;
    const filePath = path.join(directoryPath, fileName);
    const jsonData = { data: Math.random() };
    await fs.writeFile(filePath, JSON.stringify(jsonData));
    files.push(filePath);
  }
  return files;
};
