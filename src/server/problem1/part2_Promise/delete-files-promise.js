// .promises to import proomise version of fs
const fs = require('fs').promises;

// Delete a file after a delay
module.exports = function deleteFileWithDelayPromise(file, delay) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.unlink(file)
        .then(() => {
          console.log('File deleted using promise:', file);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    }, delay);
  });
};
