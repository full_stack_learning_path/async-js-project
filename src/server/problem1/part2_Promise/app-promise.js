const createDirectoryWithPromise = require('./create-directory-with-promise');
const createRandomJSONFilePromise = require('./create-random-json-files-promise');
const deleteJSONFilesWithPromise = require('./delete-files-promise');

// Step 1: Create the directory
module.exports = function runTask() {
  return createDirectoryWithPromise()
    .then(() => {
      const files = [];

      // Step 2: Create 3 random JSON files programmatically
      const createRandomFiles = [];

      for (let i = 1; i <= 3; i++) {
        createRandomFiles.push(createRandomJSONFilePromise(i));
      }

      return Promise.all(createRandomFiles).then((createdFiles) => {
        files.push(...createdFiles);
        console.log('Files created using promise:', files);

        // Step 3: Delete the files after a 3-second delay
        const deleteFilesWithPromise = files.map((file) =>
          deleteJSONFilesWithPromise(file, 5000)
        );
        return Promise.all(deleteFilesWithPromise);
      });
    })
    .catch((err) => {
      console.error('Error:', err);
    });
};
