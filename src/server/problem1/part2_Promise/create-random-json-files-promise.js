// .promises to import proomise version of fs
const fs = require('fs').promises;
const path = require('path');

const directoryPath = path.join(
  __dirname,
  '../../../public/output/async_js_exercises'
);

// Create a random JSON file
module.exports = function createRandomJSONFile(index) {
  const fileName = `file${index}.json`;
  const filePath = path.join(directoryPath, fileName);
  const jsonData = { data: Math.random() };

  return fs.writeFile(filePath, JSON.stringify(jsonData)).then(() => {
    return filePath;
  });
};
