const fs = require('fs').promises;
const path = require('path');

const directoryPath = path.join(
  __dirname,
  '../../../public/output/async_js_exercises'
);

// Create the directory if it doesn't exist
module.exports = function createDirectory() {
  return fs
    .mkdir(directoryPath, { recursive: true })
    .then(() => {
      console.log('Directory created using promise:', directoryPath);
    })
    .catch((err) => {
      throw err;
    });
};
