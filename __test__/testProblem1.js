const problem1WithCallback = require('../src/server/problem1/part1_Callback/app');
const problem1WithPromise = require('../src/server/problem1/part2_Promise/app');
const problem1WithAsync = require('../src/server/problem1/part3_Async_await/app');

// Files created and deleted using callback
problem1WithCallback();

// Files created and deleted using promise
problem1WithPromise();

// Files cretaed and deleted using async-await
problem1WithAsync();
